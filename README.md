# Definition Of Done     

## Requirements

* Provisioning the EKS cluster by Terraform
* Build and deploy HELM Chart for [freegeoip](https://github.com/fiorix/freegeoip)
* Update ReadMe

## Extras required   

* Optimized the eks cluster, reducing redundant resources and reduce the server costs.

## Done Jobs 

### Provisioning the EKS cluster by Terraform    

  I have been done to create a Terraform script for provisioning the EKS terraform-IAC repo.
  And the bellow is the eks client certificates:

```
apiVersion: v1
clusters:
- cluster:
    server: https://BC7124BA62B44C3826C4A7A962117224.yl4.ap-southeast-1.eks.amazonaws.com
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNU1EWXlNVEE0TURBek5Gb1hEVEk1TURZeE9EQTRNREF6TkZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTHhGCkNGVldCOVplemhHODBzS0cvendGS0J2dlBKdUljWlJ3TXV4d0xVWHJtcTNJenByZWZhMjUySDB4LzVtVTk5NkYKd1FMV0s1eW81WWM5SGlkeWtmYzNQYXJKNHJOZHRsZ0MyRm9FV2ZvMXlyenQyWlIrVUg0cWxDbjB1OWxHN3JJLwpEOVljTlZQZ2dxSkdGZG9hMG5aR2hoTlV3WXM0dC9QY3lWWHJ6T285R0U5RlBsdkc1WlF6QXoyYnVIbmxjUDZUCnQ4d0NwZ25OVTdFYVBLWlVNeTUxMElQd0t2cHNKZk5zU3drbmx5UjJ3UzY5RmdJc0MyQUxkRVpUcXRDM2FEaFoKUjZkT2JhVnpndkZhQ2ZjYVpaaGh0cWhta0RyUTVxcWZoZzlTbWNFVHprMTlIdDlLeFZReGRYUFZUWXF2ZkIrQQo0R29vb2pJRlVLMHJFUXM4UFEwQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFJL1FhbG5QKy9nSS83TVY0RjNHYXJBdVJmUy8KWmJHQzVxLzN1UUdIdCt2VThwTDZIREpLc0VmeVhWejFoelhFZWF0RU1OZUwrVHB1dHpJeW1JK0dUOTJSblJ5Wgo2WngxSGQzQmhtQXV3U01BTW9xd0lTQkxCelhyZ2JBNS9vdDdjUG9BVlN0eUsxT3JqQ0EwU2FiREdYd1h4b3BZCkl6SCtGREFQQllUVHFvWldRaHRmQkYzRWRxNG9WRHFiZXNPMUVaZVNGWXJ0dlludlFRN0RWeVZoRksxUVZMNG0KMFJiSVlBOUxrYUFjVWVaZlhpMFQ3azh1TldHcWVOV2JieTYvUytWcm9EZE9DN2pYakVOR21nYzBPbWdIUkxiUQo1ZnBBelNZTEZlNWk5MkdFbmtmL2RKK1pVRHExek9Nb1R0eWpsdXNtaFIvdEdGOExVSHRKNHErdjBhZz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "prod"
```

### Build and deploy HELM Chart  
It should be done to Build and Deploy (by manual) the HELM chart for freegeoip as below result.

```
curl -H "Host: geoip.local" aad9c8a3093fb11e99882062d2a741c7-337886345.ap-southeast-1.elb.amazonaws.com/json/171.250.182.207 | python -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   243  100   243    0     0    150      0  0:00:01  0:00:01 --:--:--   150
{
    "city": "Ho Chi Minh City",
    "country_code": "VN",
    "country_name": "Vietnam",
    "ip": "171.250.182.207",
    "latitude": 10.8142,
    "longitude": 106.6438,
    "metro_code": 0,
    "region_code": "SG",
    "region_name": "Ho Chi Minh",
    "time_zone": "Asia/Ho_Chi_Minh",
    "zip_code": ""
}

```

### Update ReadMe    

As README.md in terraform-IAC repo

### Optimized the eks cluster
* The idea
  - Optimize for cost by using Spot Instances.
  - The cluster must be resilient to Spot Instance interruptions.
  - The cluster must scale automatically to match the demands of an application.

* Solution  

  |Solution component|Role in solution|Deployment|
  | :--------------- | :------------- | :------- |
  | Cluster Autoscaler | Scales EC2 instances in or out | K8s pod DaemonSet on On-Demand Instances |    
  | Auto Scaling group|Provisions Spot or On-Demand Instances|Via Terraform|   
  | Spot Instance interrupt handler | Sets K8s nodes to drain state, when the Spot Instance is interrupted | K8s pod DaemonSet on all K8s nodes with the label lifecycle=EC2Spot |    

* Implementation

  - Auto Scaling group (Provisions Spot or On-Demand Instances)
    Currently the Terraform script (Terraform-IAC) is scupport to auto scaling across multiple instance types and purchase options, it support auto scaling along with the ability to control the mix of On-Demand and Spot     

```

#kubectl get node                                                         
NAME                                              STATUS    ROLES       AGE       VERSION
ip-10-0-65-185.ap-southeast-1.compute.internal    Ready     on-demand   8h        v1.12.7
ip-10-0-149-215.ap-southeast-1.compute.internal   Ready     on-demand   8h        v1.12.7
ip-10-0-179-234.ap-southeast-1.compute.internal   Ready     spot        8h        v1.12.7
ip-10-0-66-48.ap-southeast-1.compute.internal     Ready     spot        8h        v1.12.7

```


  - Spot Instance interrupt handler (Sets K8s nodes to drain state, when the Spot  Instance is interrupted) 

    * it should be done to setup the `Spot Instance interrupt handler` on current cluster, Once a termination notice is received, it will try to gracefully stop all the pods running on the Kubernetes node, up to 2 minutes before the EC2 Spot Instance backing the node is terminated.      

```
#kubectl get pod               
NAME                                                          READY     STATUS    RESTARTS   AGE
spot-termination-handler-k8s-spot-termination-handler-2wlg6   1/1       Running   0          7h
spot-termination-handler-k8s-spot-termination-handler-t76z9   1/1       Running   0          7h
spot-termination-handler-k8s-spot-termination-handler-tz7tv   1/1       Running   0          7h
spot-termination-handler-k8s-spot-termination-handler-v5crg   1/1       Running   0          7h
```

  - Cluster Autoscaler (Scales EC2 instances in or out) 
    Cluster Autoscaler is a tool that automatically adjusts the size of the Kubernetes cluster when one of the following conditions is true:    
    * there are pods that failed to run in the cluster due to insufficient resources.   
    * there are nodes in the cluster that have been underutilized for an extended period of time and their pods can be placed on other existing nodes.  
    
    * `It's not yet setup on cluster`.  
    Reason: it's really awesome solution but I am sorry because the time wasn't with me the I planned to spend the last 2 day of this weekend ( Saturday and Sunday) for worked on it but plan has changed at the end, I got some personal issue and I was unable to complete it as intended.   